﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace FastWriter
{
    public partial class MainWindow : Window
    {
        private readonly string SettingsPath = "settings.txt";
        private List<string> Settings = new List<string>();

        private int defaultSceneSeconds = 300;

        private string[] SceneNames;
        private int[] SceneSeconds;

        private int SceneCount = 5;

        private List<TextBox> SceneNameTextBoxes = new List<TextBox>();
        private List<TextBox> SceneSecondsTextBoxes = new List<TextBox>();

        private int CurrentScene = 0;
        private DateTime StartTime;
        private float CurrentTime;

        private DispatcherTimer Timer = new DispatcherTimer();
        private bool ResetFocus;

        public MainWindow()
        {
            InitializeComponent();

            // Check if settings are available and load them; I am not using application settings to reprogram the settings file from other places
            if (File.Exists(SettingsPath))
            {
                Settings = new List<string>(File.ReadAllLines(SettingsPath));
            }

            if (Settings.Count > 0)
            {
                SceneCount = int.Parse(Settings[0]);
                SceneNames = Settings[1].Split(","[0]);
                var SceneSecondsTemp = Settings[2].Split(","[0]);
                SceneSeconds = new int[SceneNames.Length];
                for (int I = 0; I < SceneSecondsTemp.Length; I++)
                {
                    SceneSeconds[I] = int.Parse(SceneSecondsTemp[I]);
                }
            }

            // Prepare timer for the writing area
            Timer.Tick += new EventHandler(CheckTime);
            Timer.Interval = new TimeSpan(0, 0, 0, 0, 100); // check every 100 milliseconds


            // Hide TabControl Headers
            foreach (TabItem _Item in ViewControl.Items)
            {
                _Item.Visibility = Visibility.Collapsed;
            }

            // Set Scene counts
            SceneCountTextField.Text = SceneCount.ToString();

            // Create the amount of TextFields 
            CreateSceneTextBoxes();
        }

        // Events

        // Change the amount of writing scenes before finishing, creates the textboxes
        private void CreateScenesButton_Click(object sender, RoutedEventArgs e)
        {
            CreateSceneTextBoxes();
        }

        // Copy the text from the output box
        private void CopyButton_Click(object sender, RoutedEventArgs e)
        {
            OutputTextArea.SelectAll();
            OutputTextArea.Copy();
            CopyButton.Focus();
        }

        // Start the writing process, start the timer
        private void StartButton_Click(object sender, RoutedEventArgs e)
        {
            OutputTextArea.Text = "";
            SetSceneVariables();
            SaveSettings();

            NextButton.Visibility = Visibility.Visible;
            FinishButton.Visibility = Visibility.Hidden;
            CurrentScene = 0;
            SetScene(CurrentScene);
            ViewControl.SelectedIndex = 1;

            ResetFocus = true;
            Timer.Start();
        }

        // proceed to the next scene, if you finish a scene quicker than the timer
        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            NextScene();
        }

        // exit the current run
        private void ExitButton_Click(object sender, RoutedEventArgs e)
        {
            Exit();
        }

        // finish the run and come back to the setup where you can copy the results
        private void FinishButton_Click(object sender, RoutedEventArgs e)
        {
            Finish();
        }

        // this function is executed every tick of the timer (100 ms), it compares the current time with the StartTime to update the progress
        // and proceeds to the next scene, if the time runs out
        private void CheckTime(object sender, EventArgs e)
        {
            CurrentTime = (float)(DateTime.Now.Subtract(StartTime).TotalMilliseconds / 1000F);
            WritingProgressBar.Value = (float)(CurrentTime / SceneSeconds[CurrentScene]) * 100F;
            WritingProgressTimeLeft.Content = Math.Ceiling(SceneSeconds[CurrentScene] - CurrentTime) + " seconds left";
            if (SceneSeconds[CurrentScene] - CurrentTime <= 0)
            {
                if (CurrentScene < SceneCount - 1)
                {
                    NextScene();
                }
                else
                {
                    Finish();
                }
            }

            // needed to set the focus to the writing area the first time you press the start button, because of the change of the TabControl
            if (ResetFocus)
            {
                WritingArea.Focus();
                ResetFocus = false;
            }
        }

        // Setup Area procedures

        // Updates the amount of textboxes and scenes in the setup area
        void CreateSceneTextBoxes()
        {
            SceneCount = int.Parse(SceneCountTextField.Text);

            if (SceneNameTextBoxes != null)
            {
                for (int I = 0; I < SceneNameTextBoxes.Count; I++)
                {
                    if (SceneNameTextBoxes[I] != null && SetupGrid.Children.Contains(SceneNameTextBoxes[I]))
                    {
                        SetupGrid.Children.Remove(SceneNameTextBoxes[I]);
                        SetupGrid.Children.Remove(SceneSecondsTextBoxes[I]);
                    }
                }
            }

            SceneNameTextBoxes = new List<TextBox>(SceneCount);
            SceneSecondsTextBoxes = new List<TextBox>(SceneCount);

            TextBox TempTextBox;

            for (int I = 0; I < SceneCount; I++)
            {
                TempTextBox = new TextBox();
                TempTextBox.Margin = new Thickness(25, 30 * I + 80, 525, 0);
                TempTextBox.Height = 25;
                TempTextBox.VerticalAlignment = VerticalAlignment.Top;
                TempTextBox.VerticalContentAlignment = VerticalAlignment.Center;
                if (SceneNames != null && SceneNames.Length > I)
                {
                    TempTextBox.Text = SceneNames[I];
                }
                else
                {
                    TempTextBox.Text = "Scene " + (I + 1);
                }
                SceneNameTextBoxes.Add(TempTextBox);
                SetupGrid.Children.Add(TempTextBox);
                TempTextBox = new TextBox();
                TempTextBox.Margin = new Thickness(185, 30 * I + 80, 425, 0);
                TempTextBox.Height = 25;
                TempTextBox.VerticalAlignment = VerticalAlignment.Top;
                TempTextBox.VerticalContentAlignment = VerticalAlignment.Center;
                TempTextBox.PreviewTextInput += new TextCompositionEventHandler((x, y) => IntFieldPreview(x, y));
                if (SceneSeconds != null && SceneSeconds.Length > I)
                {
                    TempTextBox.Text = SceneSeconds[I].ToString();
                }
                else
                {
                    TempTextBox.Text = defaultSceneSeconds.ToString();
                }
                SceneSecondsTextBoxes.Add(TempTextBox);
                SetupGrid.Children.Add(TempTextBox);
            }

            SetSceneVariables();
        }

        // updates the names and seconds of all the scenes
        private void SetSceneVariables()
        {
            SceneNames = new string[SceneCount];
            SceneSeconds = new int[SceneCount];
            for (int I = 0; I < SceneCount; I++)
            {
                if (SceneNameTextBoxes.Count > I && SceneNameTextBoxes[I] != null)
                {
                    SceneNames[I] = SceneNameTextBoxes[I].Text;
                    SceneSeconds[I] = int.Parse(SceneSecondsTextBoxes[I].Text);
                }
            }
        }

        // saves the scene count, names and seconds to configured settings path
        private void SaveSettings()
        {
            Settings.Clear();
            Settings.Add(SceneCount.ToString());
            Settings.Add(string.Join(",", SceneNames));
            Settings.Add(string.Join(",", SceneSeconds));
            File.WriteAllLines(SettingsPath, Settings);
        }

        // Writing Area procedures

        // controls the change from one scene to the next
        private void NextScene()
        {
            AppendText();
            CurrentScene++;
            SetScene(CurrentScene);
            if (CurrentScene == SceneCount - 1)
            {
                NextButton.Visibility = Visibility.Hidden;
                FinishButton.Visibility = Visibility.Visible;
            }
            WritingArea.Focus();
        }

        // completes the current run and exits to the setup area where you can copy the output
        private void Finish()
        {
            AppendText();
            Exit();
        }

        // exit to the setup area
        private void Exit()
        {
            ViewControl.SelectedIndex = 0;
            Timer.Stop();
        }

        // appends text to the output textarea in the setup area
        private void AppendText()
        {
            OutputTextArea.AppendText("!! " + WritingLabel.Text + "\n\n" + WritingArea.Text + "\n\n");
        }

        // set the properties of the current scene
        private void SetScene(int _Scene)
        {
            StartTime = DateTime.Now; // set start time to compare with the current time to get the progress bar and the seconds left correctly
            CurrentTime = SceneSeconds[_Scene];
            WritingProgressBar.Value = 0;
            WritingProgressTimeLeft.Content = Math.Ceiling(CurrentTime) + " seconds left";
            WritingLabel.Text = SceneNames[_Scene];
            WritingArea.Text = "";
        }

        // Check input events

        // Check if the user input is a number
        private void IntFieldPreview(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsNumber(e.Text);
        }

        // Check if the user paste input is a number and cancel if it is not
        private void IntFieldPasting(object sender, DataObjectPastingEventArgs e)
        {
            if (e.DataObject.GetDataPresent(typeof(string)) && IsNumber((string)e.DataObject.GetData(typeof(string))) == false)
            {
                e.CancelCommand();
            }
        }


        // Useful procedures

        private bool IsNumber(string _Input)
        {
            return _Input.All(x => char.IsDigit(x));
        }
    }
}
